import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        RouletteWheel wheel = new RouletteWheel();
        int userMoney = 1000;
        Scanner keyboard = new Scanner(System.in);

        while (userMoney > 0){
            System.out.println("Your balance is " + userMoney + "$");
            System.out.println("Would you like to bet? (yes/no): ");
            String ans = keyboard.nextLine();

            if(ans.equalsIgnoreCase("no")){
                System.out.println("Thanks for playing!");
                break;
            } else if (ans.equalsIgnoreCase("yes")) {
                System.out.println("Enter the amount of money you want to bet: ");
                int betMoney = keyboard.nextInt();
                keyboard.nextLine();

                if (betMoney > userMoney) {
                    System.out.println("Sorry you exceed your current balance. You must enter a number between 1 and " + userMoney);
                    continue;
                }

                System.out.println("Enter a number that you would like to bet on (0 - 36): ");
                int userNumber = keyboard.nextInt();
                keyboard.nextLine();

                wheel.spin();

                if (wheel.getValue() == userNumber) {
                    int winn = betMoney * 35;
                    userMoney += winn;
                    System.out.println("Congrats you won " + winn + "$!");
                } else {
                    userMoney -= betMoney;
                    System.out.println("You lost " + betMoney + "$!");
                }
            } else {
                System.out.println("You must enter yes or no!");
            }    
        }
        System.out.println("Game over! Your final balance is " + userMoney + "!");
        if (userMoney > 1000) {
            int hold = userMoney - 1000;
            System.out.println("You won " + hold + "$!"); 
        } else if (userMoney < 1000) {
            int hold = 1000 - userMoney;
            System.out.println("You lost " + hold + "$!"); 
        } else {
            System.out.println("You didnt win nor lose any money!");
        }
    }
}
